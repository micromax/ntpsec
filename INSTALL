= Installation instructions =

== Prerequisites ==

This software should build on any operating system conformant to
POSIX.1-2001 and ISO/IEC 9899:1999 (C99).  In addition, the operating
system must have either a Linux-like adjtimex(2) call or a BSD-like
ntp_adjtime(2) call. Also, it must support the IPv6 API defined in
RFC2493 and RFC2553. Finally, it must support iterating over active
UDP interfaces via getifaddrs(3) or some equivalent facility.

There are some prerequisites.  Libraries need the binary installed
to run and in addition, the development headers installed to build.

Python 2.x, x >= 6, or Python 3.x >= 3::
   Required to build, and for various scripts such as ntpviz.  Our
   Python code has been written polyglot to also run with production
   versions of Python 3.

bison::
   Required to build.
   Fedora: bison
   Debian: bison

libcap::
   Required on Linux, to support dropping root.
   Fedora: libcap and libcap-devel
   Debian: libcap2 and libcap-dev

gnuplot::
   Required for ntpviz.
   Fedora: gnuplot
   Debian: gnuplot

psutils::
   Optional for ntpviz.  Allows running with ionice()
   Gentoo: dev-python/psutil

libevent 2.x::
   Optional. Without it, ntpdig won't build. 
   Fedora: libevent and libevent-devel
   Debian: libevent and libevent-dev

   NetBSD: As installed, libevent is broken.  It links, but doesn't run.
     Here is a workaround:
      # cd /usr/lib
      # ln -s /usr/pkg/lib/libevent_core-2.0.so.5 .

seccomp::
   Optional on Linux to support restricting syscalls
   Fedora: libseccomp-devel
   Debian: libseccomp-dev (gets libseccomp2)

OpenSSL::
   Optional, required for --enable-crypto.
   Enables encryption and authentication.
   Fedora: openssl-libs and openssl-devel
   Debian: libssl1.0.0 and libssl-dev

GNU readline::
   Optional. Enhances ntpq's interactive mode by enabling line
   editing.
   Fedora: readline and readline-devel
   Debian: libreadline6 and libreadline-dev

BSD libedit::
   Optional. Enhances ntpq's interactive mode by enabling line
   editing. Used as a fallback if it is installed and GNU readline
   is not.

sys/timepps.h::
   If you are building to supply Stratum 1 time service (that is, with
   refclocks linked in) you may find that you need the sys/timepps.h
   file to be installed, depending on which refclocks you enable.
   This won't be necessary for pure client (Stratum 2 or higher)
   installations.
   Fedora: pps-tools-devel (pps-tools may be useful for debugging)
   Debian: pps-tools

asciidoc, a2x::
   You will need asciidoc to make HTML and a2x to make manual pages from the
   documentation masters.  Only required if you configured with --enable-doc.
   Note, you need asciidoc 8.6.0 at minimum.
   Fedora: asciidoc  (Stock CentOS/RHEL has only 8.4.5, you must upgrade)
   Debian: asciidoc

Local copy of DocBook stylesheets:
   Optional: Prevents documentation build failures when your Internet is down
   Fedora: docbook-xsl-stylesheets
   Debian: docbook-xsl

liberation::
   Optional, improves font quality in ntpviz renderings.
   Fedora: liberation
   Debian: liberation

The OS X build has been tested in this environment:

 OS X Version             : 10.11.x (El Capitan)
 Xcode Version            : 7.1
 Xcode Command Line Tools : 7.2-beta

All you will require is the Xcode command line tools with no additions.
There is currently no support for using Xcode's builder. NTPsec's standard
waf based build system is used.

The OS X build of NTPsec requires the OS X port of the libevent2 library:

  Site    : libevent.org
  Version : 2.0.22-stable
  Build:
   $ tar zxf libevent-2.0.22-stable.tar.gz
   $ cd libevent-2.0.22-stable
   $ ./configure --disable-openssl
   $ make
   $ sudo make install

You can use 3rd party packages such as Macports or HomeBrew for
this library if you wish and they have it available.

== Basic Installation ==

These are generic Unix installation instructions.

Under Unix, the simplest way to compile this package is:

  1. `cd' to the directory containing the package's source code and

  2. Run `./waf configure' to configure the package for your system.
  You may want to add configuration options after the verb 'configure';
  see below.

  3. Invoke `./waf build' to compile the package.

  4. Invoke `./waf install' to install the programs and any data files and
     documentation.

  5. You can uninstall cleanly by invoking `./waf uninstall' as root.

  6. You can remove the program binaries and object files from the
     source code directory by typing `./waf clean'.

  7. To also remove the files that `./waf configure' created (so you can
     configure and compile the package for a different kind of
     computer), type `./waf distclean'.

Under OS X you can build NTPsec using Xcode command line tools with no
additions. There is currently no support for using Xcode's builder.

== Qualification testing ==

Details on how to qualify NTPsec if you've never used it before
are at devel/testing.txt.

== Bugs ==

The configuration system is a work in progress, and occasionally acts
up during builds on new platforms.  If you see the message
"Compilation check failed but include exists!" this means that an attempt
to compile a small test program using the include header mentioned on
the previous line failed, but waf configure then found that despite
this the header file exists on your system.

When this happens, it is likely that the header has prerequisites
on your system that waf configure doesn't know about - that is,
other headers always need to be included before it in C programs.

Please report this as a bug, along with your platform details.

== Installation Names ==

By default, `waf install' will install the package's files in
`/usr/local/bin', `/usr/local/man', etc.  You can specify an
installation prefix other than `/usr/local' by giving waf the
option `--prefix=PATH'.

== Strict compatibility mode ==

There have been a handful of forward-incompatible changes from NTP Classic.
These are unlikely to affect normal operation.  However, there is a configure
operation, --enable-classic-mode, that restores certain legacy behaviors. This
is not recommended, as it makes the code a little bulkier and slower.

Here's what it presently does:

* Reverts logging to the old format that designates clocks with magic
  addresses rather than the driver shortname and unit number.

* Enables declaring generic-driver refclocks with the old magic-address
  syntax (but the new syntax won't work for these, though it will for
  other driver types).

* Reverts the default baudrate of the NMEA driver to 4800 (from 9600).

* Restores the old (non-RFC3339) format of logfile timestamps.

Other behaviors may be added in future releases.

== Optional Features ==

The waf builder accepts `--enable-FEATURE' options to where FEATURE
indicates an optional part of the package.  Do `waf --help' for a list
of options.

refclocks are enabled with `--refclock=<n1,n2,n3..> or --refclock=all'
`waf configure --list' will print a list of available refclocks.

== Developer options ==

--enable-debug-gdb::
     Enable GDB debugging symbols.

== Operation Controls ==

The waf builder recognizes the following options to control how it
operates.

--help::
     Print a summary of the options to `waf configure', and exit.

--version::
     Print the version of waf used to generate the `configure'
     script, and exit.

== Cross-compiling ==

Set up a cross-compile environment for the target architecture.  At minimum
it will need its own binaries for the OpenSSL and libevent2 libraries.

Configure NTPSec with:

  waf configure --enable-cross=/path/to/your/cross/cc

There are also --cross-cflags and --cross-ldflags to supply the cross compiler 
with appropriate values.
                                                                  
// end

